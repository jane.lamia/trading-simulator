﻿$(function() {
    $(".partial-content")
        .each(function(index, item) {
                var url = $(item).data("url");
                if (url && url.length > 0) {
                    $(item).load(url, function() {
                        onLoad(item);
                    });
                }
            }
        );
    //UpdateMenu();
    //setInterval(UpdateMenu, 5000);
});

//todo вернуть
//function UpdateMenu() {
//    $.ajax({
//        url: "/Messages/UnreadMessages",
//        method: 'Post',
//        success: function (result) {
//            var item = $("#chat-menu-badge");

//            if (result == 0) {
//                item.hide();
//                localStorage.setItem('unreadMessagesCount', 0);
//            } else {
//                item.show();
//                item.text(result);
//                var unreadMessagesCount = localStorage.getItem("unreadMessagesCount");
//                if (result > unreadMessagesCount) {
//                    $.ajax({
//                        url: "/Messages/GetNewMessage",
//                        method: 'GET',
//                        success: function (response) {
//                            $.notify(response.Text, { autoHide: false, className: 'info', position: "right bottom" });
//                        }
//                    });
//                }
//                localStorage.setItem('unreadMessagesCount', result);
//            }
//        }
//    });

//    $.ajax({
//        url: "/Tickets/OpenTicketsCount",
//        method: 'Post',
//        success: function (result) {
//            var itemUrgent = $("#tickets-urgent-menu-badge");
//            var itemNormal = $("#tickets-normal-menu-badge");

//            if (result == "fail") {
//                itemUrgent.show();
//                itemUrgent.text("fail");
//                itemNormal.hide();
//            } else {
//                if (result.Urgent == 0) {
//                    itemUrgent.hide();
//                } else {
//                    itemUrgent.show();
//                    itemUrgent.text(result.Urgent);
//                }

//                if (result.Normal == 0) {
//                    itemNormal.hide();
//                } else {
//                    itemNormal.show();
//                    itemNormal.text(result.Normal);
//                }
//            }
//        }
//    });
//}

function onLoad(item) {
    var funcName = $(item).data("onload");

    if (funcName && funcName.length > 0) {
        window[funcName](item);
    }
}

function onUsersLoad(item) {
    $(item).find('select[name="Role"]').on("change", OnEdit);
    $(item).find('input[name="IsActive"]').on("change", OnEdit);
    $(item).find('select[name="CoinEditionId"]').on("change", OnEdit);
}

function OnEdit() {
    this.form.elements["save"].disabled = false;
}

function OnSaveResponse(data, textStatus, xhr) {
    if (data === "") {
        window.location.reload(true);
        return;
    }

    this.elements["save"].disabled = true;
    var newForm = $("#" + this.id);
    newForm.find('select[name="Role"]').on("change", OnEdit);
    newForm.find('input[name="IsActive"]').on("change", OnEdit);
    newForm.find('select[name="CoinEditionId"]').on("change", OnEdit);
}

function OnSaveFail() {
    window.location.reload(true);
}

function initMap() {
    var myGeocoder = ymaps.geocode("Saint Petersburg");

    HintLayout = ymaps.templateLayoutFactory.createClass("<div class='my-hint'>" +
        "<span class='caption'>{{ properties.startdate }} в {{ properties.time }}</span>" +
        "<br />" +
        "<b>Адрес:</b> {{ properties.address }}" +
        "<br />" +
        "<b>Стоимость:</b> {{ properties.clientPrice }} ₽ (клинеру: {{ properties.cleanerPrice }} ₽)" +
        "<br />" +
        "<b>ID заказа:</b> {{ properties.number }}" +
        "<br />" +
        //"{% for service in properties.services %}" +
        //"{% if (service.type == 'Window') %}" +
        //"<i class='fa fa-windows' aria-hidden='true'></i>" +
        //"{% elseif (service.type == 'DishWash') %}" +
        //"<i class='fa fa-bullseye' aria-hidden='true'></i>" +
        //"{% endif %}" +
        //"{{ service.params }}&nbsp" +
        //"{% endfor %}" +
        "</div>",
        {
            // Определяем метод getShape, который
            // будет возвращать размеры макета хинта.
            // Это необходимо для того, чтобы хинт автоматически
            // сдвигал позицию при выходе за пределы карты.
            getShape: function() {
                var el = this.getElement(),
                    result = null;
                if (el) {
                    var firstChild = el.firstChild;
                    result = new ymaps.shape.Rectangle(
                        new ymaps.geometry.pixel.Rectangle([
                            [0, 0],
                            [firstChild.offsetWidth, firstChild.offsetHeight]
                        ])
                    );
                }
                return result;
            }
        }
    );

    myGeocoder.then(
        function(res) {
            myMap = new ymaps.Map("map",
            {
                center: res.geoObjects.get(0).geometry.getCoordinates(),
                zoom: 11
            });

            $.ajax('/Orders/GetOrderList',
            {
                success: OnLoadOrders,
                method: 'Post'
            });
        },
        function(err) {
            alert('Ошибка');
        }
    );
}

function OnLoadOrders(model) {
    var length = model.length;
    for (var i = 0; i < length; i++) {
        var order = model[i];
        var markType;
        switch (order.Status) {
        case 0:
            markType = 'islands#darkGreenDotIcon';
            break;
        case 1:
            markType = 'islands#blueDotIcon';
            break;
        case 2:
            markType = 'islands#yellowDotIcon';
            break;
        case 6:
            markType = 'islands#redDotIcon';
            break;
        default:
            markType = 'islands#grayDotIcon';
            break;
        }
        var myPlacemark = new ymaps.Placemark([order.Latitude, order.Longitude],
        {
            address: order.Address,
            startdate: order.StartDate,
            time: order.Time,
            number: order.Number,
            client: order.Client,
            clientPrice: order.ClientPrice,
            cleanerPrice: order.CleanerPrice
        },
        {
            hintLayout: HintLayout,
            preset: markType
        });

        myMap.geoObjects.add(myPlacemark);
    }
}

function OnLoadDialogList(item) {
    setTimeout(function() {
            UpdateDialogs();
            setInterval(UpdateDialogs, 5000);
        },
        1000);
}

function UpdateDialogs() {
    var list = $("#dialog-list .partial-content");

    list.load("/Messages/DialogList");
}

function OnLoadMessageList(item) {
    var list = $("#message-list");
    list.scrollTop(list[0].scrollHeight);

    var input = $('#chat-input');
    input.keypress(OnPressButton);

    setInterval(UpdateMessages, 5000);
}

function OnPressButton(event) {
    if (event.key === "Enter" && !event.ctrlKey) {
        SendMessage();
        return false;
    }
    if (event.key === "\n" && event.ctrlKey) {
        var input = $('#chat-input')[0];
        var caretPos = input.selectionStart;
        var text = input.value;
        input.value = text.substring(0, caretPos) + "\n" + text.substring(caretPos);
        input.selectionStart = caretPos + 1;
    }
    return true;
}

function UpdateMessages() {
    var chatList = $('#chat');
    var url = '/Messages/CheckNewMessages?id=' + chatList.data("dialog") + "&after=" + chatList.data("time");
    $.ajax({
        url: url,
        method: 'Post',
        success: function (result) {
            chatList.data("time", result.LastCheck);
            var list = result.Messages;
            var length = list.length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    var msg = list[i];
                    $('#message-' + msg.Id).remove();

                    var msgText = "";

                    var lines = msg.Text.split("\n");

                    for (var j = 0; j < lines.length; j++) {
                        var line = lines[j];
                        msgText += '<p>' + (line.length > 0 ? line : '&nbsp;') + '</p>\n';
                    }

                    chatList.append(
                    '<li id="message-' + msg.Id + '" class="' + (msg.Left ? 'left' : 'right') + ' clearfix">' +
                        '<span class="chat-img pull-' + (msg.Left ? 'left' : 'right') + '">' +
                            '<img src="' + msg.ImageUrl + '" alt="User Avatar" class="img-circle" width="50"/>' +
                        '</span>' +
                        '<div class="chat-body clearfix">' + 
                            msgText +
                            '<div class="footer">' +
                                '<i class="fa fa-clock-o fa-fw"></i> ' + msg.DateTimeView +
                            '</div>' +
                        '</div>' +
                    '</li>');
                }

                var msgList = $("#message-list");
                msgList.animate( { scrollTop: msgList[0].scrollHeight } );
            }
        }
    });
}

function SendMessage() {
    var chatList = $('#chat');
    var input = $('#chat-input');
    var url = "/Messages/SendMessage?id=" + chatList.data("dialog");

    $.ajax({
        url: url,
        method: "Post",
        data: { message: input.val() },
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        success: function(result) {
            UpdateMessages();
        }
    });
    input.val("");
}

function renderUser(data, type, full, meta) {
    //if (data.IsCash)
    //{
    //    return '<div class="user-info"><img src="' +
    //        data.Avatar +
    //        '" width="40" height="40" alt="User Avatar" class="img-circle"/> '
    //         + data.Name + ' ' + '<i class="fa fa-money ' +
    //        '" aria-hidden="true"></i>' +
    //        '</div>';
    //}
    //else
    //{
        return '<div class="user-info">'
           + data.Name +
          '</div>';
    //return '<div class="user-info"><img src="' +
    //    data.Avatar +
    //    '" width="40" height="40" alt="User Avatar" class="img-circle"/> '
    //    + data.Name +
    //    '</div>';
    //}
}



function renderLastOrder(data, type, full, meta) {
    if (data.Status == -1)
    {
        return '<div> </div>';
    }
    if (data.Status == 3)
    {
        return '<span class="status completed">Исполнен</span> <i class="fa fa-credit-card ' + (data.IsPaid ? 'paid' : 'not-paid') + '" aria-hidden="true"></i>';
    }
    else
    {
        return '<span class="status canceled">Отменен</span>';
    }  
}

function renderLevel(data, type, full, meta) {
    return data;
}

function renderNewsStatus(data, type, full, meta) {
    switch (data) {
    case 0:
        return '<span class="status active">Актуальна</span>';
    case 1:
        return '<span class="status not-active">Неактуальна</span>';
    case 2:
        return '<span class="status removed">Удалена</span>';
    default:
        return data;
    }
}


function renderStatus(data, type, full, meta) {
    switch (data) {
        case 0:
            return '<span class="status not-active">Не активен</span>';
        case 1:
            return '<span class="status active">Активен</span>';
        case 2:
            return '<span class="status blacklisted">Заблокирован</span>';
        case 3:
            return '<span class="status removed">Удалён</span>';
        default:
            return data;
    }
}

function renderLotteryStatus(data, type, full, meta) {
    switch (data) {
    case 0:
        return '<span class="status active">Приём ставок</span>';
    case 1:
        return '<span class="status selected">Ожидание розыгрыша</span>';
    case 2:
        return '<span class="status completed">Завершён</span>';
    case 3:
        return '<span class="status failed">Ошибка</span>';
    case 4:
        return '<span class="status canceled">Отменён</span>';
    case 5:
        return '<span class="status not-active">Перенесён</span>';
    default:
        return data;
    }
}

function renderRaiting(data, type, full, meta) {
    return data.toFixed(1);
}

function update(input) {
    var text = input.closest('.number-spinner').find('.text-view');
    var toString = window[input.data('tostring')];
    var val = parseFloat(input.val());
    if (typeof toString === "function")
        text.val(toString(val));
    else
        text.val(val);
};

function initNumberSpinners() {
    var inputs = $('.number-spinner input[type="hidden"]');
    var length = inputs.length;
    for (var i = 0; i < length; i++) {
        initSpinner($(inputs[i]));
    }

    $('.number-spinner button').click(function (event) {
        event.cancelBubble = true;
        if (event.stopPropagation) event.stopPropagation();
        var btn = $(this);
        var input = btn.closest('.number-spinner').find('input[type="hidden"]');
        var min = parseFloat(input.data('min'));
        var max = parseFloat(input.data('max'));
        var step = parseFloat(input.data('step'));

        var oldValue = parseFloat(input.val());
        var newVal = 0;

        if (btn.attr('data-dir') === 'up') {
            newVal = oldValue + step;
            if (newVal > max)
                newVal = max;
        } else {
            newVal = oldValue - step;
            if (newVal < min)
                newVal = min;
        }

        input.val(newVal);
        input.change();
        update(input);
    });
}

function initSpinner(input) {
    var text = input.closest('.number-spinner').find('input[type="text"]');
    update(input);

    text.focusin(function () {
        text.val(input.val());
        text.select();
    });

    text.focusout(function () {
        var min = input.data('min');
        var max = input.data('max');
        var newVal = parseInt(text.val());
        if (isNaN(newVal))
            newVal = input.val();
        else if (newVal > max)
            newVal = max;
        else if (newVal < min)
            newVal = min;

        input.val(newVal);
        update(input);
    });
}

