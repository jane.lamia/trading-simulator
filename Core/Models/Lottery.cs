﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Souvenir.Core.Models
{
    public enum LotteryStatus
    {
        BetOpened,
        BetClosed,
        Completed,
        Failed,
        Canceled,
        Transfered
    }

    public class Lottery
    {
        [Key]
        public long Id { set; get; }

        //public long LotteryScheduleId { set; get; }

        public LotteryStatus Status { set; get; }

        [Required]
        public DateTime StartBetDateTime { set; get; }

        [Required]
        public DateTime FinishBetDateTime { set; get; }

        [Required]
        public DateTime CompleteLotteryDateTime { set; get; }

        [Required]
        public int odd = 0;

        [Required]
        public int even = 0;

        public int WinCoef { set; get; }//кол-во баллов, которое начисляется за один поставленный игровой балл в случае выигрыша

        public List<Bet> Bets { set; get; }

        public double Temperature { set; get; }
    }
}
