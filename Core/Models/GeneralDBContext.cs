﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;



namespace Souvenir.Core.Models
{
    /*public class TempClass
    {
        [Key]
        public int ID { get; set; }

        public int Filed1 { get; set; }
        public int Filed2 { get; set; }
    }*/

    public class GeneralDBContext : DbContext
    {
        public static string ConnectionString { set; get; } = "DbConnection";

        /*public static GeneralDBContext Create()
        {
            var context = new GeneralDBContext(ConnectionString);
            //context.Database.Log = Logger.TraceDatabase;
            return context;
        }*/

        public GeneralDBContext() : base()
        { }

        public DbSet<Bet> Bets { get; set; }
        public DbSet<UserAccount> Users { get; set; }
        public DbSet<Lottery> Lotteries { get; set; }
        public DbSet<LotterySchedule> LotterySchedules { get; set; }

    }
}