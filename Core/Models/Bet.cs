﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;

public enum BetType
{
    even,
    odd
}

namespace Souvenir.Core.Models
{
    public class Bet
    {
        [Key]
        public long Id { set; get; }

        public long LotteryId { set; get; }

        /*    [JsonIgnore]
            public virtual Lottery Lottery { set; get; }*/

        [Required]
        public int UserId { set; get; }

        [JsonIgnore]
        [ForeignKey(nameof(UserId))]
        public virtual UserAccount User { set; get; }

        public bool IsWon { set; get; } = false;

        [Required]
        public int type;

        [Required]
        public double Amount;
    }
}
