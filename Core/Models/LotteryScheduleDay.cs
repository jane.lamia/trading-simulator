﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Souvenir.Core.Models
{
    public class LotteryScheduleDay
    {
        [Key]
        [DataMember]
        public long Id { set; get; }

        [Required]
        [DataMember]
        public DayOfWeek DayOfWeek { set; get; }

        [Required]
        [DataMember]
        [DataType(DataType.Time)]
        public TimeSpan StartTime { set; get; }

        public LotteryScheduleDay ToDtoModel()
        {
            return new LotteryScheduleDay
            {
                Id = Id,
                DayOfWeek = DayOfWeek,
                StartTime = StartTime
            };
        }
    }
}
