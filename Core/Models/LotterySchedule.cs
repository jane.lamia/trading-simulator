﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;

namespace Souvenir.Core.Models
{
    public enum LotteryScheduleStatus
    {
        [Display(Name = "Активно")]
        Active,
        [Display(Name = "Неактивно")]
        Canceled
    }

    public enum LotteryScheduleType
    {
        /// <summary>
        /// розыгрыш проводится в рамках одного тиража
        /// </summary>
        Local,
        /// <summary>
        /// розыгрыш един для всех тиражей, управляется админом
        /// </summary>
        Global
    }

    public enum Periodicity
    {
        [Display(Name = "Раз в три месяца")]
        OncePer3Months = -3,
        [Display(Name = "Раз в два месяца")]
        OncePer2Months = -2,
        [Display(Name = "Раз в месяц")]
        OncePerMonth = -1,
        [Display(Name = "Раз в две недели")]
        OncePerTwoWeeks = 0,
        [Display(Name = "Раз в неделю")]
        OncePerWeek = 1,
        [Display(Name = "2 раза в неделю")]
        TwoTimesPerWeek = 2,
        [Display(Name = "3 раза в неделю")]
        ThreeTimesPerWeek = 3,
        [Display(Name = "4 раза в неделю")]
        FourTimesPerWeek = 4,
        [Display(Name = "5 раз в неделю")]
        FiveTimesPerWeek = 5,
        [Display(Name = "6 раз в неделю")]
        SixTimesPerWeek = 6,
        [Display(Name = "Ежедневно")]
        Everyday = 7
    }

    public enum RandomGenerator
    {
        Barchart,
        Moex
    }

    [DataContract]
    public class LotterySchedule
    {
        [Key]
        public long Id { set; get; }

        [DataMember]
        public long CoinEditionId { set; get; }

        [Required]
        [DataMember]
        public LotteryScheduleStatus Status { set; get; }

        [Required]
        [DataMember]
        public Periodicity Periodicity { set; get; }

        [Required]
        [DataMember]
        public LotteryScheduleType Type { set; get; }

        [Required]
        [DataMember]
        public RandomGenerator RandomGenerator { set; get; }

        [DataMember]
        public virtual ICollection<LotteryScheduleDay> ScheduleDays { set; get; }

        [DataMember]
        public virtual ICollection<Lottery> Lotterys { set; get; }

        /// <summary>
        /// Кол-во баллов, которое начисляется за один поставленный игровой балл в случае выигрыша
        /// </summary>
        [DataMember]
        public int WinCoef { set; get; }

        [DataMember]
        public DateTime CreationDateTime { set; get; }

        [DataMember]
        public string JobId { set; get; }

        public LotterySchedule ToDtoModel()
        {
            return new LotterySchedule
            {
                Id = Id,
                JobId = JobId,
                Status = Status,
                ScheduleDays = ScheduleDays?.Select(s => s.ToDtoModel()).ToList(),
                Lotterys = Lotterys.ToList(),
                Periodicity = Periodicity,
                WinCoef = WinCoef,
                CreationDateTime = CreationDateTime
            };
        }
    }
}
