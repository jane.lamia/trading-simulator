﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;

namespace Souvenir.Core.Models
{
    public enum AccountStatus
    {
        NotActive,
        Active,
        BlackListed,
        Removed
    }

    public class UserAccount
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { set; get; }

        public virtual ICollection<Bet> Bets { set; get; }

        /*[Required]
        public AccountStatus Status { set; get; }*/

        [Required]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public string Login { set; get; }

        [Required]
        public double Balance { get; set; }

        [Required]
        public bool LoggedIn = false;

        [NotMapped]
        public string Password { get; set; }
    }
}