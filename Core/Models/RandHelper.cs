﻿using System.ComponentModel.DataAnnotations;

namespace Souvenir.Core.Models
{
    public class RandHelper
    {
        [Key]
        public long Id { get; set; }

        public long LotteryId { get; set; }

        public double Value1 { get; set; }
        public double Value2 { get; set; }
        public double Value3 { get; set; }
        public double Value4 { get; set; }
        public double Value5 { get; set; }
        public double Value6 { get; set; }
        public double Value7 { get; set; }
        public double Value8 { get; set; }
        public double Value9 { get; set; }
    }
}
