﻿using System;
using System.Net.Http;
using Newtonsoft.Json;

namespace Souvenir.Core.Weather
{
    public class OpenWeatherMap : IWeatherProvider
    {
        public static double CityNameError = 3.1415927;
        public static double InternetConnectionError = -3.1415926;

        private const string SiteName = @"https://api.openweathermap.org/data/2.5/weather";
        private const string ApiKey = "2e588898f2ede7fc41ae48f00818df3a";

        private bool ConnectionIsOk()
        {
            return true;
        }

        public double GetTemperature(string placeName)
        {
            using (var httpClient = new HttpClient())
            {
                var parameters = $"?APPID={ApiKey}&q={placeName}";
                var uri = new Uri(SiteName + parameters, UriKind.Absolute);
                var req = new HttpRequestMessage(HttpMethod.Get, uri);
                var response = httpClient.SendAsync(req);

                var resp = response.Result;
                resp.EnsureSuccessStatusCode();
                string responseBody = resp.Content.ReadAsStringAsync().Result;
                dynamic responseDynamic = JsonConvert.DeserializeObject(responseBody);

                if (responseDynamic != null)
                {
                    var tempInKelvin = responseDynamic.main?.temp.Value;
                    return Math.Round(tempInKelvin - 273.15, 1);
                }
            }
            return 0;
        }
    }
}
