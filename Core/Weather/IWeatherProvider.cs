﻿namespace Souvenir.Core.Weather
{
    public interface IWeatherProvider
    {
        double GetTemperature(string placeName);
    }
}
