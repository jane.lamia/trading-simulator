﻿namespace Souvenir.Core.Weather
{
    public class WeatherProviderFactory
    {
        public static IWeatherProvider CreateOpenWeatherMapProvider()
        {
            return new OpenWeatherMap();
        }
    }
}
