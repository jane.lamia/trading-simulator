using System;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Collections.Generic;
using Souvenir.Core.Models;

namespace Souvenir.Core.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<GeneralDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(GeneralDBContext context)
        {
            /* if (false)
             {
                 context.UserAccounts.AddOrUpdate(m => m.Login, new ManagerAccount
                 {
                     Login = "admin",
                     Password = "123",
                     Status = AccountStatus.Active,
                     Role = ManagerRole.Admin,
                     RegistrationDate = DateTime.Now,
                     LastLoginDate = DateTime.Now,
                     ManagerInfo = new ManagerInfo
                     {
                         FirstName = "����",
                         LastName = "������",
                         MiddleName = "��������",
                         Email = "ivanov@souvenir.com",
                         Phone = "+78562458447"
                     }
                 });

                 CoinEdition firstCoinEdition;
                 CoinEdition secondCoinEdition;
                 CoinEdition thirdCoinEdition;
                 CoinEdition testCE;

                 firstCoinEdition = new CoinEdition
                 {
                     FirstCoin = 1,
                     LastCoin = 99,
                     KeyWord = "12345678"
                 };

                 secondCoinEdition = new CoinEdition
                 {
                     FirstCoin = 100,
                     LastCoin = 199,
                     KeyWord = "secondedition"
                 };

                 thirdCoinEdition = new CoinEdition
                 {
                     FirstCoin = 1000,
                     LastCoin = 1255,
                     KeyWord = "testword"
                 };


                 testCE = new CoinEdition
                 {
                     FirstCoin = 2000,
                     LastCoin = 2100,
                     KeyWord = "testCE"
                 };

                 context.CoinEditions.AddOrUpdate(m => m.KeyWord, firstCoinEdition, secondCoinEdition, thirdCoinEdition,
                     testCE);

                 context.SaveChanges();

                 firstCoinEdition = context.CoinEditions.FirstOrDefault(r => r.KeyWord == "12345678");
                 secondCoinEdition = context.CoinEditions.FirstOrDefault(r => r.KeyWord == "secondedition");
                 testCE = context.CoinEditions.FirstOrDefault(r => r.KeyWord == "testCE");

                 context.UserAccounts.AddOrUpdate(m => m.Login, new ManagerAccount()
                 {
                     Login = "vasya",
                     Password = "228",
                     Status = AccountStatus.Active,
                     CoinEdition = firstCoinEdition,
                     RegistrationDate = DateTime.Now,
                     LastLoginDate = DateTime.Now,
                     ManagerInfo = new ManagerInfo
                     {
                         FirstName = "����",
                         LastName = "������",
                         MiddleName = "��������",
                         Email = "petrov@souvenir.com",
                         Phone = "+78789458147"
                     }
                 });

                 context.UserAccounts.AddOrUpdate(m => m.Login, new ManagerAccount()
                 {
                     Login = "petya",
                     Password = "228",
                     Status = AccountStatus.Active,
                     CoinEdition = secondCoinEdition,
                     RegistrationDate = DateTime.Now,
                     LastLoginDate = DateTime.Now,
                     ManagerInfo = new ManagerInfo
                     {
                         FirstName = "������",
                         LastName = "�������",
                         MiddleName = "���������",
                         Email = "sergeev@souvenir.com",
                         Phone = "+78562458125"
                     }
                 });

                 context.UserAccounts.AddOrUpdate(m => m.Login, new ManagerAccount()
                 {
                     Login = "dima",
                     Password = "228",
                     Status = AccountStatus.Active,
                     CoinEdition = thirdCoinEdition,
                     RegistrationDate = DateTime.Now,
                     LastLoginDate = DateTime.Now,
                     ManagerInfo = new ManagerInfo
                     {
                         FirstName = "�������",
                         LastName = "������",
                         MiddleName = "���������",
                         Email = "ivanovd@souvenir.com",
                         Phone = "+78562454785"
                     }
                 });


                 context.SaveChanges();

                 var clients = new List<ClientAccount>
                 {
                     new ClientAccount
                     {
                         Login = "112",
                         Password = "228",
                         Status = AccountStatus.Active,
                         CoinEdition = secondCoinEdition,
                         ClientInfo = new ClientInfo
                         {
                             //Nickname = "������� ��������",
                             Phone = "+79999999999",
                             GamePointsCount = 100,
                             Level = 1,
                             //Pin = 1234,
                             QrCode = "www.leningrad.spb.ru?fea17acb-e08d-4ac9-9b50-74dd4f329e02"
                         },
                         RegistrationDate = DateTime.Now,
                         LastLoginDate = DateTime.Now,

                     },
                     new ClientAccount
                     {
                         Login = "5",
                         Password = "228",
                         Status = AccountStatus.Active,
                         CoinEdition = firstCoinEdition,
                         ClientInfo = new ClientInfo
                         {
                             //Nickname = "Richy",
                             Phone = "+79999999888",
                             GamePointsCount = 1500,
                             Level = 2,
                             //Pin = 5678,
                             QrCode = "www.leningrad.spb.ru?fea17acb-e08d-4ac9-9b50-74dd4f329e99"
                         },
                         RegistrationDate = DateTime.Now,
                         LastLoginDate = DateTime.Now,

                     },
                     new ClientAccount
                     {
                         Login = "2001",
                         Password = "228",
                         Status = AccountStatus.Active,
                         CoinEdition = testCE,
                         ClientInfo = new ClientInfo
                         {
                             //Nickname = "RichyGuy",
                             Phone = "+79872145789",
                             GamePointsCount = 25000,
                             Level = 6,
                             //Pin = 5678,
                             QrCode = "www.leningrad.spb.ru?fea17acb-e08d-4ac9-9b50-74dd4f329e99"
                         },
                         RegistrationDate = DateTime.Now,
                         LastLoginDate = DateTime.Now,

                     },
                     new ClientAccount
                     {
                         Login = "2000",
                         Password = "228",
                         Status = AccountStatus.Active,
                         CoinEdition = testCE,
                         ClientInfo = new ClientInfo
                         {
                             //Nickname = "RichyGuy1",
                             Phone = "+79602148577",
                             GamePointsCount = 16550,
                             Level = 5,
                             //Pin = 5678,
                             QrCode = "www.leningrad.spb.ru?fea17acb-e08d-4ac9-9b50-74dd4f329e99"
                         },
                         RegistrationDate = DateTime.Now,
                         LastLoginDate = DateTime.Now,

                     }

                 };

                 context.UserAccounts.AddOrUpdate(m => m.Login, clients.ToArray());
                 context.SaveChanges();
             }

             if (!context.Settings.Any(s => s.Key == "LevelDropDays"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "����� ������� ���� ������������ ������� ���������",
                     Key = "LevelDropDays",
                     Value = 30,
                     Type = SettingType.SystemParam
                 });
             }

             if (!context.Settings.Any(s => s.Key == "GamePointsCountForPrize"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "����������� ��� ��������� ����� ���������� ������",
                     Key = "GamePointsCountForPrize",
                     Value = 1000,
                     Type = SettingType.SystemParam
                 });
             }

             if (!context.Settings.Any(s => s.Key == "PearlPrice"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "��������� ����� ��������� � ������� ������",
                     Key = "PearlPrice",
                     Value = 10,
                     Type = SettingType.SystemParam
                 });
             }

             if (!context.Settings.Any(s => s.Key == "LotteriesCountForLuckCoef"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "���������� ����������, ����������� � ������� ������������ �������",
                     Key = "LotteriesCountForLuckCoef",
                     Value = 5,
                     Type = SettingType.SystemParam
                 });
             }

             if (!context.Settings.Any(s => s.Key == "MaxDragonFeed"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "����������� �� ��������� �������",
                     Key = "MaxDragonFeed",
                     Value = 1000,
                     Type = SettingType.SystemParam
                 });
             }

             if (!context.Settings.Any(s => s.Key == "Rub"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "�����",
                     Key = "Rub",
                     Value = 10,
                     Type = SettingType.Currency
                 });
             }


             if (!context.Settings.Any(s => s.Key == "Euro"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "����",
                     Key = "Euro",
                     Value = 0.5,
                     Type = SettingType.Currency
                 });
             }

             context.SaveChanges();

             if (!context.Settings.Any(s => s.Key == "1"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "1",
                     Key = "1",
                     Value = 0,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "2"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "2",
                     Key = "2",
                     Value = 500,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "3"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "3",
                     Key = "3",
                     Value = 1500,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "4"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "4",
                     Key = "4",
                     Value = 5000,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "5"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "5",
                     Key = "5",
                     Value = 10000,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "6"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "6",
                     Key = "6",
                     Value = 20000,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "7"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "7",
                     Key = "7",
                     Value = 35000,
                     Type = SettingType.Level
                 });
             }

             if (!context.Settings.Any(s => s.Key == "8"))
             {
                 context.Settings.Add(new Setting
                 {
                     DisplayName = "8",
                     Key = "8",
                     Value = 50000,
                     Type = SettingType.Level
                 });
             }*/

            //foreach (var info in context.ClientInfo.ToList())
            //{
            //    long res;
            //    long.TryParse(info.User.Login, out res);
            //    info.CoinNum = res;
            //}

            context.SaveChanges();

            //if (false)
            //{       
            //    #region filltestschedules

            //var lotterySchedule = new LotterySchedule
            //{
            //    CoinEditionId = thirdCoinEdition.Id,
            //    CreationDateTime = new DateTime(2018, 11, 30),
            //    Periodicity = Periodicity.FiveTimesPerWeek,
            //    Status = LotteryScheduleStatus.Active,
            //    Type = LotteryScheduleType.Local,
            //    WinCoef = 15,
            //    ScheduleDays = new List<LotteryScheduleDay>
            //    {
            //        new LotteryScheduleDay {DayOfWeek = DayOfWeek.Monday, StartTime = TimeSpan.FromHours(21)},
            //        new LotteryScheduleDay {DayOfWeek = DayOfWeek.Tuesday, StartTime = TimeSpan.FromHours(21)},
            //        new LotteryScheduleDay {DayOfWeek = DayOfWeek.Wednesday, StartTime = TimeSpan.FromHours(21)},
            //        new LotteryScheduleDay {DayOfWeek = DayOfWeek.Thursday, StartTime = TimeSpan.FromHours(21)},
            //        new LotteryScheduleDay {DayOfWeek = DayOfWeek.Friday, StartTime = TimeSpan.FromHours(22)}
            //    }
            //};

            //context.LotterySchedules.AddOrUpdate(lotterySchedule);
            //context.SaveChanges();

            //var lottery = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 11, 30) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 03) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 16,
            //    SecondWinner = 12,
            //    ThirdWinner = 1,
            //    FourthWinner = 5,
            //    WinCoef = 15
            //};

            //context.Lotteries.Add(lottery);

            //var lottery2 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 03) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 04) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 7,
            //    SecondWinner = 2,
            //    ThirdWinner = 12,
            //    FourthWinner = 8,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery2);

            //var lottery3 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 04) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 05) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 14,
            //    SecondWinner = 15,
            //    ThirdWinner = 4,
            //    FourthWinner = 9,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery3);

            //var lottery4 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 05) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 06) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 4,
            //    SecondWinner = 1,
            //    ThirdWinner = 4,
            //    FourthWinner = 8,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery4);

            //var lottery5 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 06) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 07) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 11,
            //    SecondWinner = 9,
            //    ThirdWinner = 15,
            //    FourthWinner = 2,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery5);

            //var lottery6 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 07) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 10) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 10,
            //    SecondWinner = 13,
            //    ThirdWinner = 1,
            //    FourthWinner = 4,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery6);

            //var lottery7 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 07) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 10) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 10,
            //    SecondWinner = 13,
            //    ThirdWinner = 1,
            //    FourthWinner = 4,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery7);

            //var lottery8 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 10) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 11) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 11,
            //    SecondWinner = 15,
            //    ThirdWinner = 3,
            //    FourthWinner = 4,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery8);

            //var lottery9 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 11) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 12) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 10,
            //    SecondWinner = 5,
            //    ThirdWinner = 8,
            //    FourthWinner = 7,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery9);

            //var lottery10 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 12) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 13) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 1,
            //    SecondWinner = 5,
            //    ThirdWinner = 6,
            //    FourthWinner = 7,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery10);

            //var lottery11 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 13) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 14) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 10,
            //    SecondWinner = 2,
            //    ThirdWinner = 5,
            //    FourthWinner = 16,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery11);

            //var lottery12 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 14) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 17) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 3,
            //    SecondWinner = 2,
            //    ThirdWinner = 5,
            //    FourthWinner = 1,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery12);

            //var lottery13 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 17) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 18) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 1,
            //    SecondWinner = 9,
            //    ThirdWinner = 15,
            //    FourthWinner = 6,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery13);

            //var lottery14 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 18) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 19) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 10,
            //    SecondWinner = 3,
            //    ThirdWinner = 1,
            //    FourthWinner = 7,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery14);

            //var lottery15 = new Lottery
            //{
            //    LotteryScheduleId = lotterySchedule.Id,
            //    CoinEditionId = thirdCoinEdition.Id,
            //    StartBetDateTime = new DateTime(2018, 12, 19) + TimeSpan.FromHours(21),
            //    FinishBetDateTime = new DateTime(2018, 12, 20) + TimeSpan.FromHours(21),
            //    Status = LotteryStatus.BetOpened,
            //    FirstWinner = 0,
            //    SecondWinner = 0,
            //    ThirdWinner = 0,
            //    FourthWinner = 0,
            //    WinCoef = 15
            //};

            //context.Lotteries.AddOrUpdate(lottery15);

            //context.SaveChanges();

            //#endregion
            //}

            /*    if (false)
                {
                    if (!context.Faqs.Any())
                    {
                        #region Faq1

                        var faq1 = new Faq
                        {
                            Text = "������",
                            Children = new List<Faq>
                            {
                                new Faq
                                {
                                    Text = "��� ������ � ������?",
                                    TextBody =
                                        "� ������� ������ ������ ������� � ����� ������ ��������, ���� ��������� ������������, � ��� ����� ���������, ������� ����� � ������������, ������, ����������, �������, ������ � �������. �� ����� ������� ����� � �������� �������. ������ ������, ���������� ����� �������� (�� 12 ��. ������). ������� ��� � ������� ������. ���� ��������� �������� ����� ���� ��� ��������� ������ � �����, �� ��� ������ ������� � ���. �� ������������� �� ������� ����� � ��������� ������ � ��������. ����� ����������� ����������������� ������, � HOP-HOPS ���� ����������� ������ �������������� �����.",
                                    TextBodyHtml =
                                        "� ������� ������ ������ ������� � ����� ������ ��������, ���� ��������� ������������, � ��� ����� ���������, ������� ����� � ������������, ������, ����������, �������, ������ � �������. �� ����� ������� ����� � �������� �������. ������ ������, ���������� ����� �������� (�� 12 ��. ������). ������� ��� � ������� ������. ���� ��������� �������� ����� ���� ��� ��������� ������ � �����, �� ��� ������ ������� � ���. �� ������������� �� ������� ����� � ��������� ������ � ��������. ����� ����������� ����������������� ������, � HOP-HOPS ���� ����������� ������ �������������� �����.",
                                },
                                new Faq
                                {
                                    Text = "����� �� ���� ������������ - ���� ��� �������� �� ����� ������?",
                                    TextBody =
                                        "��������� ������� �� ����� ������ ������ �������� ����. ������ � ��������� ��� ������������ �������� ���-������ ��������, ���� ������ �������������� ������������ ��������.",
                                    TextBodyHtml =
                                        "��������� ������� �� ����� ������ ������ �������� ����. ������ � ��������� ��� ������������ �������� ���-������ ��������, ���� ������ �������������� ������������ ��������."
                                },
                                new Faq
                                {
                                    Text = "��� ������ � ������ �� ��������, ���� � �� ���� ��������� �������?",
                                    TextBody =
                                        "�� ������ �������� ���� � ������� ��� �������. ����� ��������, ������� �������, ������ � ���.",
                                    TextBodyHtml =
                                        "�� ������ �������� ���� � ������� ��� �������. ����� ��������, ������� �������, ������ � ���."
                                },
                                new Faq
                                {
                                    Text = "��� ����������� ��� ������ �������� � ���������?",
                                    TextBody =
                                        "������� HOP-HOPS ������� ������, ������ ������������ ���������� ������, ��������� ���������������� ����� � �����������. ��������, ������ ������������ ������ � ������ clean-�������. ������� �� ����� �� � ��� ������������. �� �� ������ ������� ����������� HOP-HOPS! ��� ���� ���� �������.",
                                    TextBodyHtml =
                                        "������� HOP-HOPS ������� ������, ������ ������������ ���������� ������, ��������� ���������������� ����� � �����������. ��������, ������ ������������ ������ � ������ clean-�������. ������� �� ����� �� � ��� ������������. �� �� ������ ������� ����������� HOP-HOPS! ��� ���� ���� �������."
                                },
                                new Faq
                                {
                                    Text =
                                        "������� ������ ������������� ������ ���� � ������, ���� � �������� ��� ������ � ����������� �������?",
                                    TextBody =
                                        "��, �������, ���� ����� � ������ � ��������� �� ���� ������-�������. � ���� ������� �������� ������, ���� � ������� ���� ���-�� �� �������. ������, ����� ���������� ����� � ��������, ����� ������ ������������, ����������� �� ������� ������� ����� � ������� ��������������. ��� ����� ������� � ��� ���� ��������� �������. �������� ������ � ���������������, ������� ��������� ��������!",
                                    TextBodyHtml =
                                        "��, �������, ���� ����� � ������ � ��������� �� ���� ������-�������. � ���� ������� �������� ������, ���� � ������� ���� ���-�� �� �������. ������, ����� ���������� ����� � ��������, ����� ������ ������������, ����������� �� ������� ������� ����� � ������� ��������������. ��� ����� ������� � ��� ���� ��������� �������. �������� ������ � ���������������, ������� ��������� ��������!"
                                },
                                new Faq
                                {
                                    Text =
                                        "��� �� ������ ������ � ����������� ������� � ������ � �������, ��������� � �����?",
                                    TextBody =
                                        "�������� ������� ����������� �� ������ � ������� ������. ���� �� ��������������� ����� �������� ����� �������� ������. ��������, ����� �������� ����������, ���� HOP-HOPS ���������� ���������������� ����� � ������������ �����������. ���������� �� ������, ����� � ������ �� ��������� �����, ������� ���������, �� ���� ���������, ������� ��������� ���� ��������� �������.",
                                    TextBodyHtml =
                                        "�������� ������� ����������� �� ������ � ������� ������. ���� �� ��������������� ����� �������� ����� �������� ������. ��������, ����� �������� ����������, ���� HOP-HOPS ���������� ���������������� ����� � ������������ �����������. ���������� �� ������, ����� � ������ �� ��������� �����, ������� ���������, �� ���� ���������, ������� ��������� ���� ��������� �������."
                                },
                                new Faq
                                {
                                    Text = "�� ��������, ��� �������� ����� �� ������. ��� ��� �����?",
                                    TextBody =
                                        "��� ��������, �������� ������ ��������������� ��������, ������������������ �� ���������������� ������/��������. � �������� ������� ������ �� ���������� ������ ������, � �� ������. ��������� ��������� � ������������, ������� ��������� � �����������, ������� ������ �������, ������������ ������������ � ����������. ����� ������ �������� �������������� ������� � ��������� � ����������� ����� ��������� �������. ",
                                    TextBodyHtml =
                                        "��� ��������, �������� ������ ��������������� ��������, ������������������ �� ���������������� ������/��������. � �������� ������� ������ �� ���������� ������ ������, � �� ������. ��������� ��������� � ������������, ������� ��������� � �����������, ������� ������ �������, ������������ ������������ � ����������. ����� ������ �������� �������������� ������� � ��������� � ����������� ����� ��������� �������. "
                                },
                                new Faq
                                {
                                    Text =
                                        "����, ����� ������ �������, ������, ������� ����� ����, ������ - � ����� ���, �� ���� ������ ���������� ������.",
                                    TextBody =
                                        "�� ���� ���, ��� ��������� � ���� ������ �� ����. ���� �� � ��� ���� ������� ���������, �� ������� HOP-HOPS ������ ��������� ����. ����� ��������, ������� ������� ������ � ������ ����������� �������������� ������ �� ��������� ������ ��� ������� ���������.",
                                    TextBodyHtml =
                                        "�� ���� ���, ��� ��������� � ���� ������ �� ����. ���� �� � ��� ���� ������� ���������, �� ������� HOP-HOPS ������ ��������� ����. ����� ��������, ������� ������� ������ � ������ ����������� �������������� ������ �� ��������� ������ ��� ������� ���������."
                                },
                                new Faq
                                {
                                    Text = "�� ������ � ������ ����� �������?",
                                    TextBody =
                                        "��� ������ ���������� �������, ��� �� ��������� ���� ��������������� �������� � �������� � ���� ����� ����� 15 ���. ���������������� �������� ������� HOP-HOPS ������ ���� �������� �������! �������� � ��� ��� ���������, ����� ������� �����.",
                                    TextBodyHtml =
                                        "��� ������ ���������� �������, ��� �� ��������� ���� ��������������� �������� � �������� � ���� ����� ����� 15 ���. ���������������� �������� ������� HOP-HOPS ������ ���� �������� �������! �������� � ��� ��� ���������, ����� ������� �����."
                                },
                                new Faq
                                {
                                    Text = "� ���� �������� ��������, ������� ���, �������� ��� ����. ������� ������?",
                                    TextBody =
                                        "��, �� ��������� � � ����� �������. ���� ���� ������� ������� ����������, ��� ������ ������ �����. � ����� ������� �� ������� �������� ��������� �������, ����� ������� ��� ������ � �����������. ���������� ���������� ������ � �������� � �������� � ����� ������ ������� ��������, ���� ������� �������. � ����� ����� ������ �������������� ������.",
                                    TextBodyHtml =
                                        "��, �� ��������� � � ����� �������. ���� ���� ������� ������� ����������, ��� ������ ������ �����. � ����� ������� �� ������� �������� ��������� �������, ����� ������� ��� ������ � �����������. ���������� ���������� ������ � �������� � �������� � ����� ������ ������� ��������, ���� ������� �������. � ����� ����� ������ �������������� ������."
                                },
                                new Faq
                                {
                                    Text =
                                        "�������� �� ���� ������ ������ � ���������� ������ � ���������, � ����� ���, ��� �������� ���������?",
                                    TextBody =
                                        "�� ������ ��������, �� � ������� ������� ����� ����� ���������� � �����������. ���������������� ������ ����������� ������� ������������� ��������� �� ���� �������� ����, ������ � ������ �������� � ������������, ������� ������� ������� ������ ������� �������� �� ����� ������� ������, �������� ����, �����, ���� � ����. � ��������� � ��� �������� ������ ���������, ������� �� �������� ��������� ������� � ������ � ���, ��� � ��� ���� �������. � �� ������� ��� �� ������ ��������� ����������. �����������, ����������������� �� ������������� ���������� ������ �������� � ����������, ����������� ������������ ����� ����� � ��������. � ������� HOP-HOPS ������ � ����� ����� ������������� � ������������ ����� ����� ������ ���������-������������������ � ������������� �����������.",
                                    TextBodyHtml =
                                        "�� ������ ��������, �� � ������� ������� ����� ����� ���������� � �����������. ���������������� ������ ����������� ������� ������������� ��������� �� ���� �������� ����, ������ � ������ �������� � ������������, ������� ������� ������� ������ ������� �������� �� ����� ������� ������, �������� ����, �����, ���� � ����. � ��������� � ��� �������� ������ ���������, ������� �� �������� ��������� ������� � ������ � ���, ��� � ��� ���� �������. � �� ������� ��� �� ������ ��������� ����������. �����������, ����������������� �� ������������� ���������� ������ �������� � ����������, ����������� ������������ ����� ����� � ��������. � ������� HOP-HOPS ������ � ����� ����� ������������� � ������������ ����� ����� ������ ���������-������������������ � ������������� �����������."
                                },
                                new Faq
                                {
                                    Text = "������ ��� ����� �������� ����� ��������?",
                                    TextBody =
                                        "� ��� ��������� ���������, ���������� � �������� �� ����� � ���������� �������, � ���������� (������ ������ ����� ��� ���� �������� ��������� ���� �� �����, ���������� ����� ������� ������) � ����������� ����������. ������ ������������� ������ HOP-HOPS �������� �������� � ����� �������� �� ���������� ������, � ����� ���������� ������� � ��������� � ��������. ��� �������� ��������� � �������� ��������, ���� ������ ������������ �������� ����������� �������� ������� ���������. ������� ��������� � ��������������� ����, ������� ����� � ������ ������ �������� HOP-HOPS. ��� ���������� ���������, ������ �������� ������������ � �������� �������� ��� ��� ������.",
                                    TextBodyHtml =
                                        "� ��� ��������� ���������, ���������� � �������� �� ����� � ���������� �������, � ���������� (������ ������ ����� ��� ���� �������� ��������� ���� �� �����, ���������� ����� ������� ������) � ����������� ����������. ������ ������������� ������ HOP-HOPS �������� �������� � ����� �������� �� ���������� ������, � ����� ���������� ������� � ��������� � ��������. ��� �������� ��������� � �������� ��������, ���� ������ ������������ �������� ����������� �������� ������� ���������. ������� ��������� � ��������������� ����, ������� ����� � ������ ������ �������� HOP-HOPS. ��� ���������� ���������, ������ �������� ������������ � �������� �������� ��� ��� ������."
                                },
                                new Faq
                                {
                                    Text = "��� ������, ���� ��� �� ����������� ������?",
                                    TextBody =
                                        "����������� �������� ���, ���� ����� �����-������ ��������. HOP-HOPS �����, ��� ��� ���������.",
                                    TextBodyHtml =
                                        "����������� �������� ���, ���� ����� �����-������ ��������. HOP-HOPS �����, ��� ��� ���������."
                                },
                                new Faq
                                {
                                    Text = "� ���-���� ���������� ������� ��� ���� ������ ������� ��� ���?",
                                    TextBody =
                                        "��� ���������, ���� � ��� ���������� �������, �� ������� ��� ����� ������ ��������.",
                                    TextBodyHtml =
                                        "��� ���������, ���� � ��� ���������� �������, �� ������� ��� ����� ������ ��������."
                                },
                                new Faq
                                {
                                    Text =
                                        "��� ������ �� ������ ����� � ����� ������ - ������ �� ������ � ������������� �������?",
                                    TextBody =
                                        "� ������ ������ HOP-HOPS �������� � �������� �����-����������. ������ �� �������� ��������� �� ������� � ���������� � ������� ���������� � ��������� �������.",
                                    TextBodyHtml =
                                        "� ������ ������ HOP-HOPS �������� � �������� �����-����������. ������ �� �������� ��������� �� ������� � ���������� � ������� ���������� � ��������� �������."
                                },
                                new Faq
                                {
                                    Text = "��� ����� �� ������� �������� � ��������?",
                                    TextBody =
                                        "�� �������� ������ � ���������������� ������ � �������� ���������� ������� ��������� �����������, ����� �������� ������������� �����������. ������� ������ ����������, ���������� � ������. ������ �������� � �������� ������������� �������. �� ��� ������� �� ��� � ����� ������ ������ �������� ���������� � �������, ������ � ������������ ������������, �������������� �� ���� ������� ��� ��������.",
                                    TextBodyHtml =
                                        "�� �������� ������ � ���������������� ������ � �������� ���������� ������� ��������� �����������, ����� �������� ������������� �����������. ������� ������ ����������, ���������� � ������. ������ �������� � �������� ������������� �������. �� ��� ������� �� ��� � ����� ������ ������ �������� ���������� � �������, ������ � ������������ ������������, �������������� �� ���� ������� ��� ��������."
                                }
                            }
                        };

                        #endregion

                        #region Faq2

                        var faq2 = new Faq
                        {
                            Text = "����� � ������",
                            Children = new List<Faq>
                            {
                                new Faq
                                {
                                    Text = "��� ��� ��������� ������?",
                                    TextBody =
                                        "���-�� ���������, � �� ������ ������� ������ � ������ ����? �� � ��������� ��������� � �������� ����� �������� � ����� �������� �� ����������. ������� ����������� �������� ������� ������ � ��������������� �� 12 ����� �� ����������� �������.",
                                    TextBodyHtml =
                                        "���-�� ���������, � �� ������ ������� ������ � ������ ����? �� � ��������� ��������� � �������� ����� �������� � ����� �������� �� ����������. ������� ����������� �������� ������� ������ � ��������������� �� 12 ����� �� ����������� �������."
                                },
                                new Faq
                                {
                                    Text =
                                        "����� ������ � ����������� ���������� ����� ��� ������ SMS � ��������. ��� �� ��� �������?",
                                    TextBody =
                                        "������� ������ ��������� ������������ ����� �����, �������� � ��� ���� �����, � ����� ��������� ��� �� ����. ����������, ��� ������ ������ ���������� ������ ����� �� ����������.",
                                    TextBodyHtml =
                                        "������� ������ ��������� ������������ ����� �����, �������� � ��� ���� �����, � ����� ��������� ��� �� ����. ����������, ��� ������ ������ ���������� ������ ����� �� ����������."
                                },
                                new Faq
                                {
                                    Text =
                                        "� �� ����� ������ ������ ���������. ���� �� � ���������� �������� ������ ����������?",
                                    TextBody =
                                        "����������, ���� ���������� ������� ����� ������ ����������. ��-������, ���, ��� ������� ���� ������ � ������� ���� �������, ������ �������� ���������. ���������� ������ ������� � ������ ������� ������ ���������, ���� ������ ������� � ������� ������ � ����������� ������� ��������������. ��������� ����� �������� � ����� ������ �������� ����� �����������. �� �����, ����� ��� ������ ��� ����������� ����������������, ������� ������ ���� ������ � ������������, �, ������ �����, ���� ��� �����. � ���� ��� �� �������� ��� ����� �������� �������� ����, � �� ��������� ������ ��� ���������� ������������ ����-������. � HOP-HOPS ������ � �������!",
                                    TextBodyHtml =
                                        "����������, ���� ���������� ������� ����� ������ ����������. ��-������, ���, ��� ������� ���� ������ � ������� ���� �������, ������ �������� ���������. ���������� ������ ������� � ������ ������� ������ ���������, ���� ������ ������� � ������� ������ � ����������� ������� ��������������. ��������� ����� �������� � ����� ������ �������� ����� �����������. �� �����, ����� ��� ������ ��� ����������� ����������������, ������� ������ ���� ������ � ������������, �, ������ �����, ���� ��� �����. � ���� ��� �� �������� ��� ����� �������� �������� ����, � �� ��������� ������ ��� ���������� ������������ ����-������. � HOP-HOPS ������ � �������!"
                                },
                                new Faq
                                {
                                    Text =
                                        "� �� ������� ������ � ����� ������ � �������� ��� �������. ����� ��� ������������ ���������?",
                                    TextBody =
                                        "�� ��� ��, ���������� ����� � ��� ��� ������! ������ ������ ������ �������� ������ ������. ������ ���������� ��� ������ ����� ���������� ������. �������� ����� � ���� ������������.",
                                    TextBodyHtml =
                                        "�� ��� ��, ���������� ����� � ��� ��� ������! ������ ������ ������ �������� ������ ������. ������ ���������� ��� ������ ����� ���������� ������. �������� ����� � ���� ������������."
                                },
                                new Faq
                                {
                                    Text = "��� �� ������������� ������������ ������ ������ �� ����� �����?",
                                    TextBody =
                                        "���������� ����������� SSL ������������ (128 bit). �� �� ������ ������ ���� ��������. ��� ���������� ������ ������������ �������������� ����� ��������� ������.",
                                    TextBodyHtml =
                                        "���������� ����������� SSL ������������ (128 bit). �� �� ������ ������ ���� ��������. ��� ���������� ������ ������������ �������������� ����� ��������� ������."
                                },
                                new Faq
                                {
                                    Text = "���� �� � �������� ������ �������?",
                                    TextBody =
                                        "������ ������� ��� ������� � ��� ��� ������������� ����� � ��� � ��������� �����. ������ ��������� ������� ��������� ������� � ������� ������ � � �� ����� �����, ��� �������� ���������.",
                                    TextBodyHtml =
                                        "������ ������� ��� ������� � ��� ��� ������������� ����� � ��� � ��������� �����. ������ ��������� ������� ��������� ������� � ������� ������ � � �� ����� �����, ��� �������� ���������."
                                },
                                new Faq
                                {
                                    Text =
                                        "����� ����, ��� ������ ��� ����� ������� � ���� ����, � �������, ����� �� ����� ����� ��� ������ ������ �������������� ������. ��� ��������?",
                                    TextBody =
                                        "��, �������� �� ���� �������, �� �������� � ���������������. ������ �� ����������� ������� ��������������� ������� ����� �������� �������. ���� ����� �������� ���������� ��� ���������� ������, �� � ������������� ��� �������.",
                                    TextBodyHtml =
                                        "��, �������� �� ���� �������, �� �������� � ���������������. ������ �� ����������� ������� ��������������� ������� ����� �������� �������. ���� ����� �������� ���������� ��� ���������� ������, �� � ������������� ��� �������."
                                }
                            }
                        };

                        #endregion

                        #region Faq3

                        var faq3 = new Faq
                        {
                            Text = "�������",
                            Children = new List<Faq>
                            {
                                new Faq
                                {
                                    Text = "��� ����� ���������������� �������� � ������� � ��� �� ������?",
                                    TextBody =
                                        "�� ���� �� ������ ������� ������������, �� � ����, ��� ���� ���������� ������, ������������ ����� ��� ����� � �������� ���������������. ������� ����� HOP-HOPS � ��� �������� �����. ������, ������� ���� � ��� �������� ������ ���������, ���� �� ��������� ��� �����������. ����� ������ ������, ��������� �� ������ ������ � HOP-HOPS, ��������� ����� � ����� ������. ���� ������� � ������� �� �������� 8 (812) 384 24 29!",
                                    TextBodyHtml =
                                        "�� ���� �� ������ ������� ������������, �� � ����, ��� ���� ���������� ������, ������������ ����� ��� ����� � �������� ���������������. ������� ����� HOP-HOPS � ��� �������� �����. ������, ������� ���� � ��� �������� ������ ���������, ���� �� ��������� ��� �����������. ����� ������ ������, ��������� �� ������ ������ � HOP-HOPS, ��������� ����� � ����� ������. ���� ������� � ������� �� �������� 8 (812) 384 24 29!",
                                },
                                new Faq
                                {
                                    Text = "��� �� ��������� � ����������� �����������?",
                                    TextBody =
                                        "��� ����� ������ � ������� � ������ ��������� �������, ��� ������ ���������. ����� ���� ���. ������ �������� �����, ������ �� �������� ��� ��������. �� ������� �� ��� ���, ���� ��������� �� ��������� ���������� ����������. ������ ��� �� ���������� ���������� ��������, � ���������� ������� ��� ����, � ������ �����. �������, ���������� �� ������� ����� �� �������, ������� HOP-HOPS ������� ���, ��� ����� ���� ����� � ������� � ��������� ����!",
                                    TextBodyHtml =
                                        "��� ����� ������ � ������� � ������ ��������� �������, ��� ������ ���������. ����� ���� ���. ������ �������� �����, ������ �� �������� ��� ��������. �� ������� �� ��� ���, ���� ��������� �� ��������� ���������� ����������. ������ ��� �� ���������� ���������� ��������, � ���������� ������� ��� ����, � ������ �����. �������, ���������� �� ������� ����� �� �������, ������� HOP-HOPS ������� ���, ��� ����� ���� ����� � ������� � ��������� ����!",
                                },
                            }
                        };

                        #endregion

                        #region Faq4

                        var faq4 = new Faq
                        {
                            Text = "������ � ������",
                            Children = new List<Faq>
                            {
                                new Faq
                                {
                                    Text = "����� �� �������� ������� ���� ������?",
                                    TextBody =
                                        "��, �������� ������ � ������� ����� � ����� ������ ��������. �������, ����� ���������� ���������� ��� ���� � ���� �������� ������� ��� ���������� ������������ �������������� �������.",
                                    TextBodyHtml =
                                        "��, �������� ������ � ������� ����� � ����� ������ ��������. �������, ����� ���������� ���������� ��� ���� � ���� �������� ������� ��� ���������� ������������ �������������� �������.",
                                },
                                new Faq
                                {
                                    Text = "������� �� �� ������ � ����-������ ��� ������?",
                                    TextBody =
                                        "��, �� ������ �������� ����� ������ � ������ ��������. �� �� ������ ������� ����������� ����������� � ������� ������� ��� �� �������� 8 (812) 424 61 74.",
                                    TextBodyHtml =
                                        "��, �� ������ �������� ����� ������ � ������ ��������. �� �� ������ ������� ����������� ����������� � ������� ������� ��� �� �������� 8 (812) 424 61 74.",
                                },
                            }
                        };

                        #endregion

                        context.Database.ExecuteSqlCommand("delete from Faqs");
                        context.SaveChanges();

                        context.Faqs.AddOrUpdate(faq1, faq2, faq3, faq4);

                        context.SaveChanges();
                    }
                }

            }*/
        }
    }
}
